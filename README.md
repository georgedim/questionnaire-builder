This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Firstly, you have to run : 

### `npm i`

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.
See the section about [running tests](#running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.

v.2 [Updated] Currently, the limits answers validation functionality works as below :

 * If you have no limits, you are able to add any number of answers and delete them.
 * If you enable the limits, you can add a minimum number of answers and a maximum.
 * If you enter a minimum value greater than the maximum value, then an error message is created.
 * If you try to enter a maximum value lower than minimum, then again an error message is created.
 * If you have a total of 4 answers with minimum value of 2 and maximum of 4, 
   then you cannot add new answer and you are able to delete up to 2.
 * If you have already a total of x answers and then you enable the limits, the answers will remain independent of the limit gap and
 an error message will be shown in order to inform the user that he has to follow the limit rules.
 
Finally, I have added two small unit tests for action and reducers using jest.
