import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import App from './App';
import configureStore from './store/configureStore';
import registerServiceWorker from './registerServiceWorker';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import './index.css';

const store = configureStore();

const jsx = (
    <Provider store={store}>
        <App/>
    </Provider>
);
ReactDOM.render(jsx, document.getElementById('root'));
registerServiceWorker();
