import { createStore, combineReducers, applyMiddleware } from 'redux';
import allReducers from '../reducers/questions';
import logger from 'redux-logger';



//Store creation
export default () => {
    const store = createStore(
        combineReducers({
            questions: allReducers
        }),
        applyMiddleware(logger)
    );

    return store;
}

