import allReducers from './questions';

test('should setup default questions', () => {
    const state = allReducers(undefined, {
        type: '@@INIT'
    });
    expect(state).toEqual([]);
});

test('should set a new question', () => {
    const state = allReducers([], {
        type: 'ADD_QUESTION',
        payload: {question: 1}
    });
    expect(state).toEqual([1]);
});

test('should set remove the question with the given id', () => {
    const state = allReducers([{id: 1},{id: 2},{id: 3}], {
        type: 'DELETE_QUESTION',
        payload: {id: 1}
    });
    expect(state).toEqual([{id: 2},{id: 3}]);
});