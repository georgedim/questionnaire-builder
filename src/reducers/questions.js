import { addQuestion, removeQuestion, updateQuestionText, moveQuestion } from '../components/question/reducer';
import { addAnswer, moveAnswer, removeAnswer } from '../components/answer/reducer';
import { setLimitValue, setDownLimit, setUpLimit} from '../components/answerLimits/reducer';

const questionReducerDefaultState = [];

export default (state = questionReducerDefaultState, action) => {
    switch (action.type) {
        case 'ADD_QUESTION' :
            return addQuestion(state, action);

        case 'DELETE_QUESTION' :
            return removeQuestion(state, action);

        case 'UPDATE_QUESTION_TEXT' :
            return updateQuestionText(state, action);

        case 'MOVE_QUESTION' :
            return moveQuestion(state, action);

        case 'ADD_ANSWER':
            return addAnswer(state, action);

        case 'MOVE_ANSWER' :
            return moveAnswer(state, action);

        case 'DELETE_ANSWER' :
            return removeAnswer(state, action);

        case 'SET_LIMIT_VALUE' :
            return setLimitValue(state, action);

        case 'SET_DOWN_LIMIT' :
            return setDownLimit(state, action);

        case 'SET_UP_LIMIT' :
            return setUpLimit(state, action);

        default:
            return state;
    }
};