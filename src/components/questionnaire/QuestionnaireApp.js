import React, { Component } from 'react';
import Question from '../question/Question';
import {Button} from  'react-bootstrap';
import {addQuestion} from '../question/actions';
import './Questionnaire.css';
import {connect} from 'react-redux';

class QuestionnaireApp extends Component {

    createQuestionComponent(question, index) {
        const firstQuestion = (index === 0);
        const lastQuestion = (index === this.props.questions.length - 1);
        question.index = ++index;

        return (
            <Question
                isFirst={firstQuestion}
                isLast={lastQuestion}
                key={question.id}
                question={question}
                index={index}
                totalQuestionsNumber={this.props.questions.length}
            />
        )
    }

    createQuestions = () => this.props.questions.map(this.createQuestionComponent.bind(this));

    render() {
        return (
          <div>
            <header className="header">
              <div>Create your Questionnaire</div>
              <Button bsSize="small"
                      onClick={this.props.addNewQuestion.bind(this)}
                      disabled={ this.props.questions.length === 10 }>Add New Question</Button>
            </header>
            <div className={`full-list ${this.props.questions.length===10 ? 'is-active' : ''}`}>
                  {this.props.questions.length===10 &&  'You reach the maximum number of Questions!!!' }
            </div>
            <div className={`empty-list ${this.props.questions.length===0 ? 'is-active' : ''}`}>
                {this.props.questions.length===0 ? 'Please enter a question' : this.createQuestions()}
            </div>
          </div>
        );
    }


}

const mapStateToProps = (state) => {
    return {questions : state.questions};
};

export default connect(
    mapStateToProps,
    {
        addNewQuestion: () => (addQuestion())
    }
)(QuestionnaireApp);
