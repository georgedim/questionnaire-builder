import React, {Component} from 'react';
import { connect } from 'react-redux';
import { updateLimit, setDownLimit, setUpLimit } from './actions';
import FontAwesome from 'react-fontawesome';
import './AnswerLimits.css';

class AnswerLimits extends Component {
    constructor(props){
        super(props);
        this.state = {
            errorInLimits: false,
            followTheLimitsRules: true
        };
    }

    componentWillReceiveProps(newProps) {
        // Setting the state for the error in Limits
        if ( newProps.limit.value &&
            newProps.limit.atLeast &&
            newProps.limit.noMoreThan &&
            parseInt(newProps.limit.atLeast, 10) > parseInt(newProps.limit.noMoreThan, 10)
        ) {
            this.setState({
                errorInLimits: true
            });
            return;
        } else {
            this.setState({
                errorInLimits : false
            });
        }

        //Setting the state for the error in following the rules
        if ( newProps.limit.value &&
            !this.state.errorInLimits &&
            (newProps.limit.atLeast || newProps.limit.noMoreThan)
        ) {
            if((newProps.limit.atLeast && newProps.answersLength < parseInt(newProps.limit.atLeast, 10)) ||
                (newProps.limit.noMoreThan && newProps.answersLength > parseInt(newProps.limit.noMoreThan, 10))
            ) {
                this.setState({
                    followTheLimitsRules: false
                });

            } else {
                this.setState({
                    followTheLimitsRules: true
                });
            }
        } else {
            this.setState({
                followTheLimitsRules : true
            });
        }
    }
    render() {
        const answerLimits = <div className="answer-limits__rules">
            <div className="answer-limits__rules--atLeast">
                <label>At least</label>
                <input type="text"
                       value={this.props.limit.atLeast || '' }
                       onChange={(e) => {
                           this.handleSetDownLimit(e);
                       }}
                />
            </div>
            <div className="answer-limits__rules--noMoreThan">
                <label>No more than</label>
                <input type="text"
                       value={ this.props.limit.noMoreThan || '' }
                       onChange={(e) => {
                           this.handleSetUpLimit(e);
                       }}
                />
            </div>
        </div>;
        return (
            <div className="answer-limits">
                <div className="answer-limits__option">
                    <input type="checkbox" id="limitAnswers"
                           checked={this.props.limit.value}
                           onChange={(e) => {
                               this.props.updateLimit(this.props.questionId, e.target.checked)
                           }}
                    />
                    <label>Limit Answers</label>
                </div>
                {this.props.limit.value && answerLimits}
                {this.state.errorInLimits && <div className="answer-limits__error">
                    <FontAwesome
                        name='exclamation'
                        size="1x"
                        style={{ color: '#707070', paddingRight: '5px' }}
                    />
                    <span>The 'no more than' value should be greater or equal than 'at least' value</span>
                    </div>}
                {!this.state.followTheLimitsRules && <div className="answer-limits__error">
                    <FontAwesome
                        name='exclamation'
                        size="1x"
                        style={{ color: '#707070', paddingRight: '5px' }}
                    />
                    <span>You should follow the Limit rules</span>
                </div>}
            </div>
        );
    };

    handleSetDownLimit(ev) {
        const atLeast = ev.target.value;
        if (atLeast.match(/^\d*$/)) {
            this.props.setDownLimit(this.props.questionId, atLeast);
        }
    }

    handleSetUpLimit(ev) {
        const noMoreThan = ev.target.value;
        if (noMoreThan.match(/^\d*$/)) {
            this.props.setUpLimit(this.props.questionId, noMoreThan);
        }
    }
}

const mapStateToProps = (state, ownProps) => {
    const { limit } = state.questions.find(({id}) => id === ownProps.questionId);
    const answersLength = state.questions.find(({id}) => id === ownProps.questionId)['answers'].length;
    return { limit, answersLength };
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateLimit: (id, value) => {
            dispatch(updateLimit(id, value))
        },
        setDownLimit: (id, value) => {
            dispatch(setDownLimit(id, value))
        },
        setUpLimit: (id, value) => {
            dispatch(setUpLimit(id, value))
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(AnswerLimits);