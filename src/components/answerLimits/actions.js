//limit answer
export const updateLimit = ( questionId, limitValue) => ({
    type: 'SET_LIMIT_VALUE',
    payload: {
        questionId,
        limitValue
    }
});

//set down limit
export const setDownLimit = ( questionId, limitValue) => ({
    type: 'SET_DOWN_LIMIT',
    payload: {
        questionId,
        limitValue
    }
});

//set up limit
export const setUpLimit = ( questionId, limitValue) => ({
    type: 'SET_UP_LIMIT',
    payload: {
        questionId,
        limitValue
    }
});