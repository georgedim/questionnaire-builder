export const setLimitValue = (state, action) => {
    return state.map(question => {
        if (question.id === action.payload.questionId) {
            question.limit = {
                ...question.limit,
                value: action.payload.limitValue
            };
        }
        return question;
    });
};

export const setDownLimit = (state, action) => {
    return state.map(question => {
        if (question.id === action.payload.questionId) {
            question.limit = {
                ...question.limit,
                atLeast: action.payload.limitValue
            };
        }
        return question;
    });
};

export const setUpLimit = (state, action) => {
    return state.map(question => {
        if (question.id === action.payload.questionId) {
            question.limit = {
                ...question.limit,
                noMoreThan: action.payload.limitValue
            };
        }
        return question;
    });
};