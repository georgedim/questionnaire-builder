import shortid from 'shortid';

//Add question
export const addQuestion = (
    {
        question_text = '',
        answers = [],
        limit = {
            value: false,
            atLeast: undefined,
            noMoreThan: undefined
        }
    } = {}) => ({
    type: 'ADD_QUESTION',
    payload: {
        question: {
            id: shortid.generate(),
            question_text,
            answers,
            limit
        }
    }
});

//delete question
export const deleteQuestion = ({ id } = {}) => ({
    type: 'DELETE_QUESTION',
    payload: {
        id
    }
});

//move question
export const moveQuestion = (question, direction) => ({
    type: 'MOVE_QUESTION',
    payload: {
        question,
        direction
    }
});

//update question text
export const updateQuestionText = (id, update) => ({
    type: 'UPDATE_QUESTION_TEXT',
    payload: {
        id,
        update
    }
});