import { addQuestion, deleteQuestion, moveQuestion, updateQuestionText } from './actions';

test('should setup add question action object with provided values', () => {
    const answer = {
        question_text : '123',
        answers : [],
        limit : {
            value: true,
            atLeast: 1,
            noMoreThan: 2
        }
    };
    const action = addQuestion(answer);
    expect(action).toEqual({
        type: 'ADD_QUESTION',
        payload: {
            question: {
                ... answer,
                id: expect.any(String)
            }
        }
    })
});

test('should setup add question action object with default values', () => {
    const answer = {
        question_text : '',
        answers : [],
        limit : {
            value: false,
            atLeast: undefined,
            noMoreThan: undefined
        }
    };
    const action = addQuestion();
    expect(action).toEqual({
        type: 'ADD_QUESTION',
        payload: {
            question: {
                ... answer,
                id: expect.any(String)
            }
        }
    })
});

test('should setup remove question action object', () => {
    const action = deleteQuestion({id: '1'});
    expect(action).toEqual({
        type: 'DELETE_QUESTION',
        payload: {
            id: '1'
        }
    })
});

test('should setup move question action object', () => {
    const action = moveQuestion({question:'question'}, 2);
    expect(action).toEqual({
        type: 'MOVE_QUESTION',
        payload: {
            question: {
                question: 'question'
            },
            direction: 2
        }
    })
});

test('should setup updateQuestionText question action object', () => {
    const action = updateQuestionText(1, 'test');
    expect(action).toEqual({
        type: 'UPDATE_QUESTION_TEXT',
        payload: {
            id: 1,
            update: 'test'
        }
    })
});