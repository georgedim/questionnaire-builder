import React from 'react';
import {connect} from 'react-redux';
import QuestionLabel from '../questionLabel/QuestionLabel';
import AnswerLimits from './../answerLimits/AnswerLimits';
import AddAnswer from "../addAnswer/AddAnswer";
import AnswersList from './../answersList/AnswersList';
import {updateQuestionText, moveQuestion, deleteQuestion} from './actions';
import FontAwesome from 'react-fontawesome';
import './Question.css';

const canMoveQuestion = (totalQuestions, index, direction) => {
    const oldIndex = --index;
    return oldIndex + direction >= 0 && oldIndex + direction < totalQuestions ;
};

const Question = (props) => {
    return (
        <div className="question-card">
            <QuestionLabel label={'Question'} counter={props.index}/>
            <div className="question-card__question">
                <input className="question-card__question--input" type="text"
                       placeholder="Enter your question prompt" value={props.question.question_text}
                       onChange={(e) => {
                           props.updateQuestionText(props.question.id, e.target.value)
                       }}
                />
                <div className="question-card__question--actions">
                    {(() => {
                        if (!props.isFirst) {
                            return (
                                <div className="question-card__question--actions__arrow-up">
                                    <FontAwesome
                                        name='angle-up'
                                        size="2x"
                                        onClick={()=>{
                                            if(canMoveQuestion(props.totalQuestionsNumber, props.index, -1)) {
                                                return props.moveQuestion(props.question,-1);
                                            }
                                        }}
                                        style={{ color: '#707070' }}
                                    />
                                </div>
                            )
                        }
                    })()}
                    {(() => {
                        if (!props.isLast) {
                            return (
                                <div className="question-card__question--actions__arrow-down">
                                    <FontAwesome
                                        name='angle-down'
                                        size="2x"
                                        onClick={()=> {
                                            if (canMoveQuestion(props.totalQuestionsNumber, props.index, 1)) {
                                                props.moveQuestion(props.question, 1)
                                            }
                                        }}
                                        style={{ color: '#707070' }}
                                    />
                                </div>
                            )
                        }
                    })()}
                    <div className="question-card__question--actions__trash">
                        <FontAwesome
                            name='trash-o'
                            size="2x"
                            onClick={()=>{
                                props.deleteQuestion(props.question)
                            }}
                            style={{ color: '#707070' }}
                        />
                    </div>
                </div>
            </div>

            <AnswerLimits
                questionId={props.question.id}
            />
            <div>
                <AnswersList
                    questionId={props.question.id}
                />
                <AddAnswer
                    {...props.question}
                />
            </div>
        </div>

    );
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateQuestionText: (id, value) => {
            dispatch(updateQuestionText(id, value))
        },
        moveQuestion: (id, direction) => {
            dispatch(moveQuestion(id, direction))
        },
        deleteQuestion: (question) => {
            dispatch(deleteQuestion(question))
        },
    }
};

export default connect(() => ({}), mapDispatchToProps)(Question);
