
export const addQuestion = (state, action) => {
    return [
        ...state,
        action.payload.question
    ];
};

export const removeQuestion = (state, action) => {
    return state.filter(({ id }) => id !== action.payload.id);
};

export const updateQuestionText = (state, action) => {
    return state.map(question => {
        if (question.id === action.payload.id) {
            return {
                ...question,
                question_text: action.payload.update
            }
        } else {
            return question;
        }
    });
};

export const moveQuestion = (state, action) => {
    const oldIndex = state.findIndex(question => question === action.payload.question);
    const newIndex = (oldIndex + action.payload.direction);
    let arrayClone = [...state];
    arrayClone.splice(oldIndex,1);
    arrayClone.splice(newIndex,0,action.payload.question);
    return arrayClone;
};