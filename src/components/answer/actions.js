import shortid from 'shortid';

//add answer
export const addAnswer = ( questionId, answer_text) => ({
    type: 'ADD_ANSWER',
    payload: {
        questionId,
        answer: {
            id: shortid.generate(),
            answer_text
        }
    }
});

//move answer
export const moveAnswer = ( questionId, answerId, direction) => ({
    type: 'MOVE_ANSWER',
    payload: {
        questionId,
        answerId,
        direction
    }
});

//delete answer
export const deleteAnswer = ( questionId, answerId) => ({
    type: 'DELETE_ANSWER',
    payload: {
        questionId,
        answerId
    }
});