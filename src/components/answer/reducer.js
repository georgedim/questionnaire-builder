export const addAnswer = (state, action) => {
    return state.map(question => {
        if (question.id === action.payload.questionId) {
            question.answers = [...question.answers, action.payload.answer];
        }
        return question;
    });
};

export const removeAnswer = (state, action) => {
    return state.map(question => {
        if (question.id === action.payload.questionId) {
            question.answers = question.answers.filter(({id}) => id !== action.payload.answerId);
        }
        return question;
    });
};

export const moveAnswer = (state, action) => {
    return state.map( question => {
        if (question.id === action.payload.questionId) {
            const oldIndex = question.answers.findIndex(answer => answer.id === action.payload.answerId);
            const answer = question.answers[oldIndex];
            const newIndex = (oldIndex + action.payload.direction);
            let arrayClone = [...question.answers];
            arrayClone.splice(oldIndex, 1);
            arrayClone.splice(newIndex, 0, answer);
            question.answers = arrayClone;
        }
        return question;
    });
};