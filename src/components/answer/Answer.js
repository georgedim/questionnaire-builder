import React from 'react';
import { connect } from 'react-redux';
import { moveAnswer, deleteAnswer } from './actions';
import FontAwesome from 'react-fontawesome';
import './Answer.css';


const canRemoveAnswer = (totalAnswers, limits) => {
    return (!limits.value || !limits.atLeast || (limits.value && limits.atLeast < totalAnswers));
};

const canMoveAnswer = (totalAnswers, index, direction) => {
    const oldIndex = --index;
    return oldIndex + direction >= 0 && oldIndex + direction < totalAnswers ;
};

const Answer = (props) => {
        return (
            <div className="answer__details">
                <div>
                    <span>{props.index}</span>
                    <label className="answer__details--input">{props.label}</label>
                </div>
                <div className="answer__details--actions">
                    <div className="answer__details--actions__arrow-up">
                        <FontAwesome
                            name='angle-up'
                            size="2x"
                            onClick={(e) => {
                                if (canMoveAnswer(props.totalAnswers, props.index, -1)) {
                                    return props.moveAnswer(props.questionId, props.id, -1);
                                }
                            }}
                            style={{ color: '#707070' }}
                        />
                    </div>
                    <div className="answer__details--actions__arrow-down">
                        <FontAwesome
                            name='angle-down'
                            size="2x"
                            onClick={(e) => {
                                if(canMoveAnswer(props.totalAnswers, props.index, 1)) {
                                   return props.moveAnswer(props.questionId, props.id, 1);
                                }
                            }}
                            style={{ color: '#707070' }}
                        />
                    </div>
                    <div className="answer__details--actions__trash">
                        <FontAwesome
                            name='trash-o'
                            size="2x"
                            onClick={(e) => { 
                                if( canRemoveAnswer(props.totalAnswers,props.limit) ) {
                                    return props.deleteAnswer(props.questionId, props.id);
                                }
                            }}
                            style={{ color: '#707070' }}
                        />
                    </div>
                </div>
            </div>
        )
};

const mapDispatchToProps = (dispatch) => {
    return {
        moveAnswer: (id, answerId, direction) => {
            dispatch(moveAnswer(id, answerId, direction))
        },
        deleteAnswer: (id, answerId) => {
            dispatch(deleteAnswer(id, answerId))
        }
    }
};

export default connect(()=>({}),mapDispatchToProps)(Answer);
