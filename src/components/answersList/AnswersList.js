import React from 'react';
import { connect } from 'react-redux';
import Answer from './../answer/Answer';


const AnswersList = (props) => {
    const availableAnswers = props.answers.map((item, index) => {
        return (
            <Answer
                key={item.id}
                label={item.answer_text}
                id={item.id}
                index={++index}
                questionId={props.questionId}
                totalAnswers={props.answers.length}
                limit={props.limit}
            />
        )
    });
    return (
        <div>
            {availableAnswers}
        </div>
    )
};

const mapStateToProps = (state, ownProps) => {
    const {answers, limit} = state.questions.find(({id}) => id === ownProps.questionId);
    return {answers, limit};
};

export default connect(mapStateToProps)(AnswersList);
