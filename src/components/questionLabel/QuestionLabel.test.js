import React from 'react';
import { shallow } from 'enzyme';
import QuestionLabel from './QuestionLabel';

test('should render QuestionLabel with the given props', ()=>{
    const wrapper = shallow(<QuestionLabel label={'test'} counter={1}/>);
    expect(wrapper.find('.question__label').text()).toBe('test 1');
});

test('should render QuestionLabel with the given props-snapshot', ()=>{
    const wrapper = shallow(<QuestionLabel label={'test'} counter={1}/>);
    expect(wrapper).toMatchSnapshot();
});