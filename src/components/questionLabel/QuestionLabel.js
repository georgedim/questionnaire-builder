import React from 'react';
import './QuestionLabel.css';

const Counter = (props) => {
    return (
        <div className="question__label" >
            {props.label} {props.counter}
        </div>
    )
};


export default Counter;
