import React from 'react';
import './AddAnswer.css';
import { connect } from 'react-redux';
import { addAnswer } from '../answer/actions';


const canAddAnswer = (totalAnswers, limits) => {
    return ( !limits.value || !limits.noMoreThan || (limits.value && limits.noMoreThan > totalAnswers) ) ;
};

const AddAnswer = (props) => {
    return(
        <div className="addAnswer">
            <input className="addAnswer--input" type="text" placeholder="Add a potential answer here..." name="answer"
                onKeyPress={(ev) => {
                    if ( ev.key === 'Enter' && ev.target.value && canAddAnswer(props.answers.length, props.limits) ) {
                        ev.preventDefault();
                        props.addAnswer(props.id, ev.target.value);
                        ev.target.value='';
                    }
                }}
            />
        </div>
    )
};


export default connect((state, ownProps) => ({
    limits: state.questions.find(question => question.id === ownProps.id).limit,
    answers: state.questions.find(question => question.id === ownProps.id).answers
}), {
    addAnswer: (answerId, answerText) => (addAnswer(answerId, answerText))
})(AddAnswer);
