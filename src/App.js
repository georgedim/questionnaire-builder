import React from 'react';
import QuestionnaireApp from './components/questionnaire/QuestionnaireApp';

const App = () => {
    return (
      <div className="container">
        <QuestionnaireApp/>
      </div>
    );
};

export default App;
